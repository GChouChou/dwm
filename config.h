/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 16;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {
    "Iosevka Nerd Font:size=10",
    "Misc Ohsnap:size=13",
};
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";

static const char col_fg[]          = "#ffffff";
static const char col_bg[]          = "#000000";
static const char col_fg_alt[]      = "#cccccc";
static const char col_bg_alt[]      = "#222222";
static const char *colors[][3]      = {
    /*               fg             bg          border   */
    [SchemeNorm] = { col_fg_alt,    col_bg_alt, col_bg_alt },
    [SchemeSel]  = { col_fg,        col_bg,     col_fg_alt },
};

/* tagging */
static const unsigned int ntags = 5;    /* max index of tags that is always shown */
static const char *tags[] = {
    "1", "2", "3", "4", "5",
    "6", "7", "8", "9"
};

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class                    instance    title       tags mask   isfloating  monitor */
    { NULL,                     "utils/",   NULL,       0,          1,          -1 },

    { "Qalculate-gtk",          NULL,       NULL,       0,          1,          -1 },
    { "Nm-connection-editor",   NULL,       NULL,       0,          1,          -1 },

    /* Browsers */
    { "Chromium",               NULL,       NULL,       1 << 1,     0,          -1 },
    { "Google-chrome",          NULL,       NULL,       1 << 1,     0,          -1 },
    { "firefox",                NULL,       NULL,       1 << 1,     0,          -1 },
    { "qutebrowser",            NULL,       NULL,       1 << 1,     0,          -1 },

    { "Gimp",                   NULL,       NULL,       1 << 4,     1,          -1 },

    { "discord",                NULL,       NULL,       1 << 8,     0,          -1 },
    //{ "Zathura",                NULL,       NULL,       1 << 3,     0,          -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
    /* symbol   arrange function */
    { "[=]",    tile },    /* first entry is default */
    { "/~/",    NULL },    /* no layout function means floating behavior */
    { "[M]",    monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(...) { .v = (const char*[]){__VA_ARGS__, NULL} }

/* commands */
static const char term[]                = "st";
static const char systemctl[]           = "system.sh";
static const char audioctl[]            = "volume.sh";
static const char brightnessctl[]       = "backlight.sh";
static const char kbdbrightnessctl[]    = "asus-kbd-backlight";
static const char screenshotctl[]       = "screenshot.sh";

static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[] = { term, NULL };
static const char *browsercmd[] = { "firefox", NULL };

static Key keys[] = {
    /* modifier                     key         function        argument */
    { MODKEY,                       XK_space,   spawn,          {.v = dmenucmd } },
    { MODKEY,                       XK_Return,  spawn,          {.v = termcmd } },
    { MODKEY|ShiftMask,             XK_Return,  spawn,          SHCMD( "emacsclient", "-c" ) },
    { MODKEY|ControlMask|ShiftMask, XK_Return,  spawn,          SHCMD( "emacsclient", "-e", "(server-start nil 1)" ) },
    { MODKEY,                       XK_q,       killclient,     {0} },

    /* Movement */
    { MODKEY,                       XK_j,       focusstack,     {.i = +1 } },
    { MODKEY,                       XK_k,       focusstack,     {.i = -1 } },
    { MODKEY,                       XK_l,       focusstack,     {.i = +1 } },
    { MODKEY,                       XK_Right,   focusstack,     {.i = +1 } },
    { MODKEY,                       XK_h,       focusstack,     {.i = -1 } },
    { MODKEY,                       XK_Left,    focusstack,     {.i = -1 } },

    { MODKEY,                       XK_b,       togglebar,      {0} },

    /* Stack modifiers */
    { MODKEY|ShiftMask,             XK_i,       incnmaster,     {.i = +1 } },
    { MODKEY|ShiftMask,             XK_o,       incnmaster,     {.i = -1 } },
    { MODKEY|ShiftMask,             XK_u,       setmfact,       {.f = -0.05} },
    { MODKEY|ShiftMask,             XK_p,       setmfact,       {.f = +0.05} },

    /* Window management */
    { MODKEY,                       XK_grave,   zoom,           {0} },
    { MODKEY,                       XK_Tab,     view,           {0} },
    { MODKEY,                       XK_e,       setlayout,      {.v = &layouts[0]} }, // tile
    { MODKEY,                       XK_w,       setlayout,      {.v = &layouts[1]} }, // float
    { MODKEY,                       XK_f,       setlayout,      {.v = &layouts[2]} }, // monocle
    { MODKEY,                       XK_t,       setlayout,      {0} },
    { MODKEY|ShiftMask,             XK_f,       togglefloating, {0} },


    /* Screenshot */
    /**
     * No mod: selection screen
     * Shift: whole screen
     * Control: window
     * Control + Shift: selection
     * Modkey: clipboard
     */
    { 0,                            XK_Print,   spawn,          SHCMD( screenshotctl ) },
    { MODKEY,                       XK_Print,   spawn,          SHCMD( screenshotctl, "clip" ) },
    { ShiftMask,                    XK_Print,   spawn,          SHCMD( screenshotctl, "file", "screen" ) },
    { ControlMask,                  XK_Print,   spawn,          SHCMD( screenshotctl, "file", "window" ) },
    { ControlMask|ShiftMask,        XK_Print,   spawn,          SHCMD( screenshotctl, "file", "select" ) },
    { MODKEY|ShiftMask,             XK_Print,   spawn,          SHCMD( screenshotctl, "clip", "screen" ) },
    { MODKEY|ControlMask,           XK_Print,   spawn,          SHCMD( screenshotctl, "clip", "window" ) },
    { MODKEY|ControlMask|ShiftMask, XK_Print,   spawn,          SHCMD( screenshotctl, "clip", "select" ) },


    /* Special keys */
    { 0,             XF86XK_AudioLowerVolume,   spawn,          SHCMD( audioctl, "down", "1" ) },
    { 0,             XF86XK_AudioRaiseVolume,   spawn,          SHCMD( audioctl, "up", "1" ) },
    { ShiftMask,     XF86XK_AudioLowerVolume,   spawn,          SHCMD( audioctl, "down", "10" ) },
    { ShiftMask,     XF86XK_AudioRaiseVolume,   spawn,          SHCMD( audioctl, "up", "10" ) },
    { 0,                    XF86XK_AudioMute,   spawn,          SHCMD( audioctl, "mute" ) },

    { 0,            XF86XK_MonBrightnessDown,   spawn,          SHCMD( brightnessctl, "slow", "down", "10" ) },
    { 0,              XF86XK_MonBrightnessUp,   spawn,          SHCMD( brightnessctl, "slow", "up", "10" ) },
    { ShiftMask,    XF86XK_MonBrightnessDown,   spawn,          SHCMD( brightnessctl, "fast", "down", "5" ) },
    { ShiftMask,      XF86XK_MonBrightnessUp,   spawn,          SHCMD( brightnessctl, "fast", "up", "5" ) },

    { 0,            XF86XK_KbdBrightnessDown,   spawn,          SHCMD( kbdbrightnessctl, "down" ) },
    { 0,              XF86XK_KbdBrightnessUp,   spawn,          SHCMD( kbdbrightnessctl, "up" ) },


    /* Utils and shortcuts */
    { MODKEY,                       XK_g,       spawn,          {.v = browsercmd } },
    { MODKEY,                       XK_r,       spawn,          SHCMD( term, "-e", "ranger" ) },
    { MODKEY|ShiftMask,             XK_r,       spawn,          SHCMD( term, "-e", "rtv" ) },
    { MODKEY,                       XK_z,       spawn,          SHCMD( term, "-e", "calcurse" ) },
    { MODKEY|ShiftMask,             XK_z,       spawn,          SHCMD( term, "-e", "neomutt" ) },

    // { MODKEY,                       XK_c,       spawn,          SHCMD( "clipmenu" ) },
    { MODKEY,                       XK_c,       getStackWID,     SHCMD( "/home/jyu/dotfiles/dwm/makeTabbed.sh") },
    // { MODKEY,                       XK_c,       spawn,          SHCMD( "xsetroot", "-name", "\"", (const char *) getStackWID(), "\""  ) },

    { MODKEY,                       XK_i,       spawn,          SHCMD( term, "-n", "utils/top", "-e", "htop" ) },
    { MODKEY|ShiftMask,             XK_c,       spawn,          SHCMD( term, "-n", "utils/calc", "-e", "R", "-q", "--no-save", "--no-restore" ) },
    { MODKEY,                       XK_n,       spawn,          SHCMD( term,  "-e", "nvim", "-c", "FZF" ) },


    /* Tags and monitors */
    { MODKEY,                       XK_comma,   focusmon,       {.i = -1 } },
    { MODKEY,                       XK_period,  focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,             XK_comma,   tagmon,         {.i = -1 } },
    { MODKEY|ShiftMask,             XK_period,  tagmon,         {.i = +1 } },

    { MODKEY,                       XK_0,       view,           {.ui = ~0 } },
    { MODKEY|ShiftMask,             XK_0,       tag,            {.ui = ~0 } },
    TAGKEYS(                        XK_1,                       0),
    TAGKEYS(                        XK_2,                       1),
    TAGKEYS(                        XK_3,                       2),
    TAGKEYS(                        XK_4,                       3),
    TAGKEYS(                        XK_5,                       4),
    TAGKEYS(                        XK_6,                       5),
    TAGKEYS(                        XK_7,                       6),
    TAGKEYS(                        XK_8,                       7),
    TAGKEYS(                        XK_9,                       8),

    { MODKEY|ControlMask|ShiftMask, XK_q,       quit,           {0} },
    { MODKEY,                       XK_Escape,  spawn,          SHCMD( systemctl ) },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
